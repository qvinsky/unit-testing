let mongoose = require("mongoose"); // import mongoose
let {
  update
} = require('../models'); // import transaksi models

//Require the dev-dependencies
let chai = require('chai'); // import chai for testing assert
let chaiHttp = require('chai-http'); // make virtual server to get/post/put/delete
const assertArrays = require('chai-arrays');
let server = require('../index.js'); // import app from index
let should = chai.should(); // import assert should from chai
let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiYmI4OWI4YjAtNzg3Ny00NWZlLWIwMWUtMjI3NWMxMzYzM2M2IiwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIn0sImlhdCI6MTYxMjQ0NTU1N30.1U12rpNNvi2cyABBW4UarHHmNE2oDmI7nx-d1N4eOOE"
let getUser = {
  profile_image: 'https://talikasih.kuyrek.com:3000/img/null',
  id: 'bb89b8b0-7877-45fe-b01e-2275c13633c6',
  name: 'test',
  email: 'test@test.com',
  bank_name: null,
  bank_account_number: null,
  createdAt: '2021-02-03T08:18:32.000Z'
}
let getCampaign = {
  images: 'https://talikasih.kuyrek.com:3001/img/',
  view: null,
  total_donation: 0,
  total_donation_rupiah: 0,
  total_share: 0,
  status: 'pending',
  wallet: 0,
  deleted: false,
  _id: '601bf7805151c038af1ee1e9',
  title: '<title>',
  goal: 50000,
  due_date: '2121-12-12T00:00:00.000Z',
  category: '<category>',
  story: '<story>',
  user: {
    profile_image: 'https://talikasih.kuyrek.com:3000/img/null',
    id: 'bb89b8b0-7877-45fe-b01e-2275c13633c6',
    name: 'test',
    email: 'test@test.com',
    bank_name: null,
    bank_account_number: null,
    createdAt: '2021-02-03T08:18:32.000Z'
  },
  created_at: '2021-02-04T13:32:48.191Z',
  updated_at: '2021-02-04T13:32:48.191Z',
  id: '601bf7805151c038af1ee1e9'
}


chai.use(chaiHttp); // use chaiHttp
chai.use(assertArrays); 

describe('Update', () => {
  beforeEach((done) => { //Before each test we empty the database
    update.remove({}, (err) => {
      done();
    });
  });
})

  /*
   * Test the /GET route
   */
  describe('/GET update', () => {
    it('it should GET all withdraw', (done) => {
      chai.request(server) // request to server (index.js)
        .get('/update/get/withdraw')
        .end((err, res) => {
          res.should.have.status(200); // Response should have status 200
          res.body.should.be.an('object'); // Body Response should be an object
          res.body.should.have.property('status'); // Body Response should have 'status' property
          res.body.should.have.property('data'); // Body Response should have 'data' property
          res.body.data.should.be.an('array'); // Body Response .data should be an array
          res.body.data.should.be.ofSize(res.body.data.length)
          done();
        });
    });
  });

  describe('/GET all update', () => {
    it('it should GET all', (done) => {
      chai.request(server) // request to server (index.js)
        .get('/update/get/all')
        .end((err, res) => {
          res.should.have.status(200); // Response should have status 200
          res.body.should.be.an('object'); // Body Response should be an object
          res.body.should.have.property('status'); // Body Response should have 'status' property
          res.body.should.have.property('data'); // Body Response should have 'data' property
          res.body.data.should.be.an('array'); // Body Response .data should be an array
          // res.body.data.should.be.ofSize(res.body.data.length)
          done();
        });
    });
  });

describe('/POST update', () => {
  it('it should POST a campaign update with amount', (done) => {
    chai.request(server)
      .post('/update/create/withdraw')
      .set({
        Authorization: `Bearer ${token}`
      })
      .send({
        amount: 0,
        message: "message",
        campaign_id: "601bf7805151c038af1ee1e9",
        user : getUser,
        campaign : getCampaign
      })
      .end((err, res) => {
        res.should.have.status(200); // Response should have status 200
        res.body.should.be.an('object'); // Body Response should be an object
        res.body.should.have.property('status'); // Body Response should have 'status' property
        res.body.should.have.property('data'); // Body Response should have 'data' property
        res.body.data.should.be.an('object'); // Body Response .data should be an array
        done()
      })
  })
})

describe('/POST update', () => {
  it('it should POST a campaign update without amount', (done) => {
    chai.request(server)
      .post('/update/create')
      .set({
        Authorization: `Bearer ${token}`
      })
      .send({
        message: "message",
        campaign_id: "601bf7805151c038af1ee1e9",
        user : getUser,
        campaign : getCampaign
      })
      .end((err, res) => {
        res.should.have.status(200); // Response should have status 200
        res.body.should.be.an('object'); // Body Response should be an object
        res.body.should.have.property('status'); // Body Response should have 'status' property
        res.body.should.have.property('data'); // Body Response should have 'data' property
        res.body.data.should.be.an('object'); // Body Response .data should be an array
        // res.body.data.should.have.property('_id'); // data {_id: ....}
        done()
      })
  })
})

describe('/GET user update', () => {
  it('it should GET a user update', (done) => {
    chai.request(server)
      .get('/update/get/user')
      .set({
        Authorization: `Bearer ${token}`
      })
      .query({
        user_id: "bb89b8b0-7877-45fe-b01e-2275c13633c6"
      })
      .end((err, res) => {
        res.should.have.status(200); // Response should have status 200
        res.body.should.be.an('object'); // Body Response should be an object
        res.body.should.have.property('status'); // Body Response should have 'status' property
        res.body.should.have.property('data'); // Body Response should have 'data' property
        res.body.data.should.be.an('array'); // Body Response .data should be an array
        // res.body.data.should.have.property('_id'); // data {_id: ....}
        done()
      })
  })
})

describe('/GET campaign update', () => {
  it('it should GET a campaign update', (done) => {
    chai.request(server)
      .get('/update/get/')
      .set({
        Authorization: `Bearer ${token}`
      })
      .query({
        campaign_id: "601bf7805151c038af1ee1e9"
      })
      .end((err, res) => {
        res.should.have.status(200); // Response should have status 200
        res.body.should.be.an('object'); // Body Response should be an object
        res.body.should.have.property('status'); // Body Response should have 'status' property
        res.body.should.have.property('data'); // Body Response should have 'data' property
        res.body.data.should.be.an('array'); // Body Response .data should be an array
        // res.body.data.should.have.property('_id'); // data {_id: ....}
        done()
      })
  })
})