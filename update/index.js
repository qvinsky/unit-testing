const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const updateRoutes = require ('./routes/updateRoutes.js')
const app = express();
const axios = require('axios');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(cors());

app.use(express.static('public'));
app.use('/update', updateRoutes)

app.listen(3003, () => {
    console.log('update running on port 3003');
})

// exports.method = app;
// // exports.otherMethod = function() {};
// // exports.secondMethod = function() {};

module.exports = app;

// module.exports = 
// otherMethod: getGitUser((user) => {
//       return axios
//        .get(`https://api.github.com/users/${user}`)
//        .then((result) => result.data)
//        .catch((err) => console.err(err));
//       });

// exports.secondMethod = {
//     getUser(username) {
//       return axios
//         .get(`https://api.github.com/users/${username}`)
//         .then(res => res.data)
//         .catch(error => console.log(error));
//     }
//   };
// module.exports = {
//     getGitUser(user) {
//       return axios
//        .get(`https://api.github.com/users/${user}`)
//        .then((result) => result.data)
//        .catch((err) => console.err(err));
//       }
//    };

// exports.method =
//     getGitUser((user) => {
//       return axios
//        .get(`https://api.github.com/users/${user}`)
//        .then((result) => result.data)
//        .catch((err) => console.err(err));
//       });

//    const a =    getGitUser((user) => {
//     return axios
//      .get(`https://api.github.com/users/${user}`)
//      .then((result) => result.data)
//      .catch((err) => console.err(err));
//     })
